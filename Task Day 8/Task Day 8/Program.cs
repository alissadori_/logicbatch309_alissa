﻿using System;
using System.Globalization;

namespace Task_Day_8
{
    internal class Program
    {
        static void MenuSoal()
        {
            int pil;
            Console.Clear();

            do
            {
                Console.WriteLine("----- Menu Soal Day 8 -----");
                Console.WriteLine("1. Soal 1");
                Console.WriteLine("2. Soal 2");
                Console.WriteLine("3. Soal 3");
                Console.WriteLine("4. Soal 4");
                Console.WriteLine("5. Soal 5");

                Console.WriteLine();

                Console.Write("Masukkan Pilihan = "); pil = int.Parse(Console.ReadLine());

                Console.WriteLine();

                switch (pil)
                {
                    case 1:
                        Soal1();
                        break;
                    case 2:
                        Soal2();
                        break;
                    case 3:
                        Soal3();
                        break;
                    case 4:
                        Soal4();
                        break;
                    case 5:
                        Soal5();
                        break;
                    default:
                        Console.WriteLine("Menu tidak tersedia");
                        break;
                }
            } while (pil != 5);
        }

        static void Soal1()
        {
            int biayaParkir = 3000;
            int totalParkir = 0;

            Console.Clear();

            Console.Write("Masukkan waktu masuk   = ");
            string waktuMasuk1 = Console.ReadLine();
            DateTime waktuMasuk2 = DateTime.Parse(waktuMasuk1);


            Console.Write("Masukkan waktu keluar  = ");
            string waktuKeluar1 = Console.ReadLine();
            DateTime waktuKeluar2 = DateTime.Parse(waktuKeluar1);

            Console.WriteLine();

            TimeSpan lamaParkir = (waktuKeluar2 - waktuMasuk2);

            if (lamaParkir.Minutes > 0)
            {
                totalParkir = ((int)lamaParkir.Hours + 1) * biayaParkir;
            }
            else
            {
                totalParkir = (int)lamaParkir.Hours * biayaParkir;
            }

            Console.WriteLine($"Total biaya parkir selama {lamaParkir.Hours}:{lamaParkir.Minutes} adalah {totalParkir}");

            Console.WriteLine();
            Console.WriteLine();
        }

        static void Soal2()
        {
            Console.Clear();

            Console.Write("Masukkan tanggal pinjam  = ");
            string tglPinjam1 = Console.ReadLine();
            DateTime tglPinjam2 = DateTime.Parse(tglPinjam1);

            DateTime deadline = tglPinjam2.AddDays(3);

            Console.Write("Masukkan tanggal kembali = ");
            string tglKembali1 = Console.ReadLine();
            DateTime tglKembali2 = DateTime.Parse(tglKembali1);

            TimeSpan telat = tglKembali2 - deadline;

            int denda = 500, totalDenda = 0;

            Console.WriteLine();

            if (tglKembali2 <= deadline)
            {
                Console.WriteLine("Anda tidak perlu membayar denda karena sudah mengembalikkan buku tepat waktu");
            }
            else
            {
                totalDenda = (int)telat.Days * denda;
                Console.WriteLine($"Anda telat mengembalikkan buku selama {telat.Days} hari, sehingga anda dikenai denda sebesar Rp{totalDenda}");
            }

            Console.WriteLine();
            Console.WriteLine();
        }

        static void Soal3()
        {
            Console.Clear();

            Console.Write("Tanggal mulai (dd/mm/yyyy)= ");
            DateTime tanggalMulai = DateTime.Parse(Console.ReadLine());

            Console.Write("Lama kelas = ");
            int lamaKelas = int.Parse(Console.ReadLine());

            Console.Write("Hari Libur = ");
            string hariLibur = Console.ReadLine();
            int[] libur = Array.ConvertAll(hariLibur.Split(","), int.Parse);

            for (int i = 0; i < 10; i++)
            {
                DayOfWeek hari = tanggalMulai.DayOfWeek;
                int hari = (int)tanggalMulai.DayOfWeek;

                if (hari == 6 || hari = 0)
                {
                    i--;
                }
                else if (Array.IndexOf(libur, tanggalMulai.Day) != -1)
                {
                    i--;
                }

                tanggalMulai = tanggalMulai.AddDays(1);
            }

            Console.WriteLine("Kelas akan ujian pada : " + tanggalMulai);

            Console.WriteLine();
            Console.WriteLine();
        }

        static void Soal4()
        {
            Console.Write("Input waktu masuk = ");
            string masuk = Console.ReadLine();
            DateTime masuk1 = DateTime.Parse(masuk);

            Console.Write("Input sewa (jam)  = ");
            int jam = int.Parse(Console.ReadLine());

            DateTime sewa1 = masuk1.AddHours(jam);
            TimeSpan lamaMain = sewa1 - masuk1;

            int hargaSewa = 3500, bayar = 0;
            bayar = (int)lamaMain.Hours * hargaSewa;

            Console.WriteLine($"Anak tersebut selesai bermain warnet pada pukul {sewa1.ToString("HH:mm")}, dan total biayanya adalah {bayar}");

            Console.WriteLine();
            Console.WriteLine();

            int pil;
            do
            {
                Console.Write("Apakah anda ingin menambah durasi bermain ? (1.Y /2.T) = ");
                pil = int.Parse(Console.ReadLine());

                if (pil == 2)
                {
                    break;
                }

                Console.Write("Input tambah (jam)  = ");
                int jam1 = int.Parse(Console.ReadLine());

                sewa1 = sewa1.AddHours(jam1);

                lamaMain = sewa1 - masuk1;

                bayar = (int)lamaMain.Hours * hargaSewa;

                Console.WriteLine($"Anak tersebut selesai bermain warnet pada pukul {sewa1.ToString("HH:mm")}, dan total biayanya adalah {bayar}");

            } while (pil == 1);

            Console.WriteLine();
            Console.WriteLine();
        }

       static void Soal5()
        {
            Console.Clear();

            CultureInfo ci = new CultureInfo("id-ID");

            Console.Write("Tanggal lahir (dd/mm/yyyy) = ");
            string tglLahir1 = Console.ReadLine();
            DateTime tglLahir2 = DateTime.Parse(tglLahir1, ci);


            DateTime now = DateTime.Now;

            TimeSpan umur1 = now - tglLahir2;

            int umur2 = (int)umur1.Days / 365;
            int biayaKonser = 1500000;

            if (umur2 >= 18)
            {
                if ((tglLahir2.Day == now.Day) && (tglLahir2.Month == now.Month))
                {
                    Console.WriteLine($"Umur Anda    = {umur2}");
                    Console.WriteLine("Selamat ulang tahun, konser gratis untukmu");
                    Console.WriteLine($"Biaya konser = {biayaKonser * 0}");
                    Console.WriteLine("Silahkan melakukan pembayaran");
                }
                else
                {
                    Console.WriteLine($"Umur Anda    = {umur2}");
                    Console.WriteLine($"Biaya konser = {biayaKonser}");
                    Console.WriteLine("Silahkan melakukan pembayaran");
                }
            }
            else
            {
                Console.WriteLine($"Umur anda = {umur2}");
                Console.WriteLine("Maaf anda belum cukup umur untuk menonton konser ini");
            }

            Console.WriteLine();
            Console.WriteLine();
        }


        static void Main(string[] args)
        {
            Program pg = new Program();
            pg.MenuSoal();
        }
    }
}
