﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_Day_7
{
    internal class Program
    {
        public void MenuSoal()
        {
            int pil;
            Console.Clear();

            do
            {
                Console.WriteLine("----- Menu Soal Day 7 -----");
                Console.WriteLine("1. Soal 1");
                Console.WriteLine("2. Soal 2");
                Console.WriteLine("3. Soal 3");
                Console.WriteLine("4. Soal 4");
                Console.WriteLine("5. Soal 5");
                Console.WriteLine("6. Soal 6");
                Console.WriteLine("7. Soal 7");
                Console.WriteLine("8. Keluar");

                Console.WriteLine();

                Console.Write("Masukkan Pilihan = "); pil = int.Parse(Console.ReadLine());

                Console.WriteLine();

                switch (pil)
                {
                    case 1:
                        Soal1();
                        break;
                    case 2:
                        Soal2();
                        break;
                    case 3:
                        Soal3();
                        break;
                    case 4:
                        Soal4();
                        break;
                    case 5:
                        Soal5();
                        break;
                    default:
                        Console.WriteLine("Menu tidak tersedia");
                        break;
                }
            } while (pil != 5);
        }


        public void Soal1() // soal kode julius caesar
        {
            Console.Clear();

            Console.Write("Input kata   = "); string kata1 = Console.ReadLine();
            Console.Write("Input rotasi = "); int rot = int.Parse(Console.ReadLine());

            char[] kata2 = kata1.ToCharArray();

            int pengubah;
            int c = kata2[0] + 2;
            char symbol;

            if(rot>26)
            {
                rot = rot % 26;
            }

            for(int i = 0; i < kata2.Length; i++)
            {
                if (kata2[i] == '-')
                {
                    continue;
                }

                //kondisi huruf kecil
                else if (kata2[i] >= 97 && kata2[i] <= 122)
                {
                    if (kata2[i] + rot >= 122)
                    {
                        pengubah = kata2[i] + rot - 26;
                    }
                    else
                    {
                        pengubah = kata2[i] + rot;
                    }
                    symbol = (char)pengubah;
                    kata2[i] = symbol;
                }

                else if (kata2[i] >= 65 && kata2[i] <=90)
                {
                    if (kata2[i] + rot >= 90)
                    {
                        pengubah = kata2[i] + rot - 26;
                    }
                    else
                    {
                        pengubah = kata2[i] + rot;
                    }
                    symbol = (char)pengubah;
                    kata2[i] = symbol;

                }
            }

            string cetak = string.Join("", kata2);
            Console.WriteLine(cetak);

            Console.WriteLine();
        }


        public void Soal2()
        {
            Console.Clear();

            Console.Write("Masukkan kata    = ");
            string kata = Console.ReadLine();

            Console.Write("Masukkan panjang = ");
            int[] panjang = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            char[] alfabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

            int[] simpanP = new int[panjang.Length];
            int max = 0;

            for(int i = 0; i < kata.Length; i++)
            {
                char h = kata[i];
                int index = Array.IndexOf(alfabet, h);

                int p = panjang[index];
                simpanP[i] = p;

                if(max < p)
                {
                    max = p;
                } // kalau pakai ternary --> max = max < p ? p : max;
            }

            Array.Sort(simpanP);
            Array.Reverse(simpanP);

            Console.WriteLine($"Nilai tertinggi       = {max}");
            Console.WriteLine($"Panjang teks          = {kata.Length}");
            Console.WriteLine($"{max} x {kata.Length} = {max*kata.Length}");
            Console.WriteLine();
        }


        public void Soal3() // mengeluarkan vokal dan konsonan pada kalimat
        {
            int i, j;
            string vokal1="", konsonan1="";
            char tempVokal, tempKonsonan;
            char[] vokal2, konsonan2;

            Console.Clear();

            Console.Write("Input kalimat = ");
            string kalimat1 = Console.ReadLine();

            for (i = 0; i < kalimat1.Length; i++)
            {
                if ((kalimat1[i] >= 'a' && kalimat1[i] <= 'z') || (kalimat1[i] >= 'A' && kalimat1[i] <= 'Z'))
                {
                    if (kalimat1[i] == 'A' || kalimat1[i] == 'a' || kalimat1[i] == 'I' || kalimat1[i] == 'i' || kalimat1[i] == 'U' || kalimat1[i] == 'u' || kalimat1[i] == 'E' || kalimat1[i] == 'e' || kalimat1[i] == 'O' || kalimat1[i] == 'o')
                    {
                         vokal1 += kalimat1[i].ToString();
                    }
                    else
                    {
                        konsonan1 += kalimat1[i].ToString();
                    }
                }
            }

            vokal2 = vokal1.ToCharArray();
            konsonan2 = konsonan1.ToCharArray();

            for(i=0; i<vokal2.Length;i++)
            {
                for(j=0; j<vokal2.Length-1; j++)
                {
                    if (vokal2[j] > vokal2[j+1])
                    {
                        tempVokal = vokal2[j];
                        vokal2[j] = vokal2[j + 1];
                        vokal2[j + 1] = tempVokal;
                    }
                }
            }

            for(i=0; i<konsonan2.Length; i++)
            {
                for(j=0; j<konsonan2.Length-1; j++)
                {
                    if (konsonan2[j] > konsonan2[j+1])
                    {
                        tempKonsonan= konsonan2[j];
                        konsonan2[j] = konsonan2[j+1];
                        konsonan2[j+1] = tempKonsonan;
                    }
                }
            }

            Console.WriteLine($"Vokal    = {String.Join("",vokal2)}");
            Console.WriteLine($"Konsonan = {String.Join("",konsonan2)}");

            Console.WriteLine();
            Console.WriteLine();
        }


        public void Soal4()
        {
            Console.Clear();


        }


        public void Soal5()
        {
            Console.Clear();


        }



        static void Main(string[] args)
        {
            Program pg = new Program();
            pg.MenuSoal();
        }
    }
}