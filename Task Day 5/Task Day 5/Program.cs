﻿using System;
using System.Runtime.ConstrainedExecution;

namespace Task_Day_5
{
    internal class Program
    {
        public void Soal1() //Menampilkan blangan prima(Input = 7, Output = 2,3,5)
        {
            int x, i, j,batas ;
            
            Console.Clear();

            Console.Write("Masukkan batas angka = "); batas=int.Parse(Console.ReadLine()); int[] angka = new int[batas];

            for (i=1; i < batas; i++)
            {
                x = 0;
                for(j=1; j<=i; j++)
                {
                    if (i % j == 0)
                    {
                        x = x + 1;
                    }
                }

                if(x==2)
                {
                    Console.Write($"{i} ");
                }  
            }

            Console.WriteLine();
            Console.WriteLine();
        }

        public void Soal2() 
        {
            int[,] angka = new int[3, 3];
            int[] input = new int[100];
            int i, j, d1 = 0, d2 = 0;

            Console.WriteLine("Input data matriks = ");

            for (i = 0; i < 3; i++)
            {
                input = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

                for(j = 0; j<3; j++)
                {
                    angka[i, j] = input[j];
                }

                Console.WriteLine(" ");
            }

            for(i = 0; i < 3; i++)
            {
                d1 += angka[i, i];
                d2 += angka[i, 3 - 1 - i];
            }

            Console.WriteLine($"{d1} - {d2} = {d1 - d2}");

        }

        public void Soal3()
        {
            string kalimat;
            int countKapital=0, countU=0;

            Console.Clear();

            Console.Write("Masukkan kalimat = "); kalimat=Console.ReadLine();

            foreach(char huruf in kalimat)
            {
                if (huruf >= 'A' && huruf <= 'Z')
                {
                    countKapital++;
                }

                if(huruf=='u' ||huruf=='U')
                {
                    countU++;
                }
            }
            
            Console.WriteLine($"Huruf u ada       = {countU}");
            Console.WriteLine($"Huruf kapital ada = {countKapital}");

            Console.WriteLine();


        }

        public void Soal4() //pertama kalimatnya di split dulu, abis itu di looping per kalimat, lalu di looping per kata
        {
            int i;
            Console.Clear();

            Console.Write("Masukkan kalimat = "); string kalimat1 = Console.ReadLine(); //aku sayang kamu
            string[] kalimat2 = kalimat1.Split(" ");

            foreach (string kalimat3 in kalimat2)
            {
                //Console.WriteLine(kalimat3); //aku[0] sayang [1] kamu[2]

                char[] kalimat4 = kalimat3.ToCharArray();

                for (i = 0; i <= kalimat4.Length; i++)
                {
                    if (i != 0 && i != kalimat4.Length)
                    {

                        Console.Write("*"); //a[0] k[1] u[2]
                    }
                    else
                    {
                        Console.Write(kalimat4[i]);
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public void Soal5()
        {
            int i;
            Console.Clear();

            Console.Write("Masukkan kalimat = "); string kalimat1 = Console.ReadLine(); //aku sayang kamu
            string[] kalimat2 = kalimat1.Split(" ");

            foreach (string kalimat3 in kalimat2)
            {
                //Console.WriteLine(kalimat3); //aku[0] sayang [1] kamu[2]

                char[] kalimat4 = kalimat3.ToCharArray();

                for (i = 0; i <= kalimat4.Length; i++)
                {
                    if (i != 0 && i != kalimat4.Length)
                    {
                        Console.Write(kalimat4[i]);
                    }
                    else
                    {
                        Console.Write("*"); //a[0] k[1] u[2]
                    }


                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public void Soal6() //Mengambil 3 kata terakhir dalam suatu kalimat. Belum siap
        {
            int i;
            Console.Clear();

            Console.Write("Masukkan kalimat = ");
            string kalimat1 = Console.ReadLine();
            string[] kalimat2 = kalimat1.Split(" ");

            for(i = 0; i < kalimat2.Length; i++)
            {
                for(int j = 0; j < kalimat2[i].Length; j++)
                {
                    if(j >= kalimat2[i].Length-3)
                    {
                        Console.Write(kalimat2[i][j]);
                    }
                }
                Console.Write(" ");
            }
            
            Console.WriteLine();
            Console.WriteLine();

        }

        public void Soal7() //sorting bubble sort
        {
            
            int[] arr = new int[100];
            int n, i, j, x;

            Console.Clear();

            Console.Write("Masukkan banyak data = "); n=int.Parse(Console.ReadLine()) ;
                    
            Console.WriteLine("Masukkan isi array = ");
            for (i = 0; i < n; i++)
            {
                arr[i]=int.Parse(Console.ReadLine()) ;
            }

            Console.Write("Array sebelum disorting = ");
            for(i=0;i<n;i++)
            {
                Console.Write(arr[i] + " ");
            }
                            
            for(i = 0; i < n; i++)
            {
                for(j=0; j < n-i-1; j ++)
                {
                    if(arr[j] > arr[j+1])
                    {
                        x = arr[j];
                        arr[j] = arr[j+1];
                        arr[j+1] = x;
                    }
                }
            }
            
            Console.WriteLine();
            
            Console.Write("Array setelah disortir = ");
            for (i = 0; i < n; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine();

        }

        public void Soal8() //program balik huruf
        {
            int Length=0;
            string kalimat="", balik="";

            Console.Clear();

            Console.Write("Masukkan kalimat = ");
            kalimat=Console.ReadLine();

            Length=kalimat.Length-1;
            while(Length>=0)
            {
                balik=balik+kalimat[Length];
                Length--;
            }

            Console.WriteLine();

            if(kalimat==balik)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }

            Console.WriteLine();
            Console.WriteLine();
            
        }

        public void Soal9()
        {
            Console.Write("Masukkan jumlah uang = "); int uang = int.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Harga Baju = "); string[] baju = Console.ReadLine().Split(',');
            int[] baju2 = Array.ConvertAll(baju, int.Parse);
            Console.WriteLine();

            Console.Write("Harga Celana"); string[] celana = Console.ReadLine().Split(',');
            int[] celana2 = Array.ConvertAll(celana, int.Parse);

            int max = 0;

            for(int i = 0; i < baju.Length; i++)
            {
                for(int j = 0; j<celana2.Length; j++)
                {
                    int prices = baju2[i] + celana2[i];
                    if(prices>=max)
                    {
                        max = prices;
                    }
                }
               

            }

            Console.WriteLine();
            Console.WriteLine();
        }

        public void Soal10()
        {
            int i,j,k,n;

            Console.Write("Masukkan banyak bintang = "); n=int.Parse(Console.ReadLine());

            for(i=1;i<=n;i++)
            {
                for(j=1;j<=n-i;j++)
                {
                    Console.Write(" ");
                }
            
                for(j=1;j<=2*i-1;j++)
                {
                    Console.Write("*");
                }
                
                Console.WriteLine();
            }

            for(i=1;i<n;i++)
            {
                for(j=1;j<=i;j++)
                {
                    Console.Write(" ");
                }
                
                for(j=2*n-2*i-1;j>=1;j--)
                {
                    Console.Write("*");
                }
                
                Console.WriteLine();
            }

        }

        static void Main(string[] args)
        {
            int pil;
            Program pg = new Program();

            do
            {
                Console.WriteLine("----- Menu Soal -----");
                Console.WriteLine("1. Soal 1");
                Console.WriteLine("2. Soal 2");
                Console.WriteLine("3. Soal 3");
                Console.WriteLine("4. Soal 4");
                Console.WriteLine("5. Soal 5");
                Console.WriteLine("6. Soal 6");
                Console.WriteLine("7. Soal 7");
                Console.WriteLine("8. Soal 8");
                Console.WriteLine("9. Soal 9");
                Console.WriteLine("10. Soal 10");
                Console.WriteLine();
               
                Console.Write("Masukkan Pilihan = "); pil = int.Parse(Console.ReadLine());

                Console.WriteLine();

                switch (pil)
                {
                    case 1:
                        pg.Soal1();
                        break;
                    case 2:
                        pg.Soal2();
                        break;
                    case 3:
                        pg.Soal3();
                        break;
                    case 4:
                        pg.Soal4();
                        break;
                    case 5:
                        pg.Soal5();
                        break;
                    case 6:
                        pg.Soal6();
                        break;
                    case 7:
                        pg.Soal7();
                        break;
                    case 8:
                        pg.Soal8();
                        break;
                    case 9:
                        pg.Soal9();
                        break;
                    case 10:
                        pg.Soal10();
                        break;
                    default:
                        Console.WriteLine("Menu tidak tersedia");
                        break;
                }
            } while (pil != 10);














            /*Console.Write("Input angka = "); string angka=Console.ReadLine();
            //10,20,30,40,50

            string[] angka2 = angka.Split(',');
            int[] angka3 = Array.ConvertAll(angka2, int.Parse);

            Console.WriteLine(angka3[0] + 10);

            //10+20+30+40=100
            int jumlah=0;
            foreach(int jumlah2 in angka3)
            {
                jumlah += jumlah2;
            }

            string cetak = string.Join(" + ", angka3);
            Console.WriteLine($"{cetak} = {jumlah}");*/
        }
    }
}
