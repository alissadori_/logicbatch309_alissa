﻿using System;

namespace Task_Day_1
{
    internal class Program
    {
        public double LuasLingkaran(double r)
        {
            double luas = 3.14 * r * r;

            return luas;
        }

        public double KllgLingkaran(double r)
        {
            double kllg = 2 * 3.14 * r;

            return kllg;
        }

        public int LuasPersegi(int s)
        {
            int luas = s * s;
            return luas;
        }

        public int KllgPersegi(int s)
        {
            int kllg = s * s;
            return kllg;
        }

        public int ModAngka(int a, int p)
        {
            int hasil = a % p;

            return hasil;
        }


        public int Rokok(int x)
        {
            int hasil = x / 8;

            return hasil;
        }


        static void Main(string[] args)
        {
            Program pg = new Program();

            Console.WriteLine("***** TASK DAY 1 *****");
            Console.WriteLine("1. Buat program untuk mencari luas dan keliling lingkaran");
            Console.WriteLine("2. Buat program untuk mencari luas dan keliling persegi");
            Console.WriteLine("3. Buat program untuk mencari hasil modulus adalah 0");
            Console.WriteLine("4. Buat program untuk mencari hasil batang rokok pemulung dan penghasilannya");

            Console.WriteLine();
            Console.Write("Pilih Soal = ");
            int pil = int.Parse(Console.ReadLine());

            if (pil == 1)
            {
                Console.WriteLine();
                Console.WriteLine("---LINGKARAN---");
                Console.Write("Input jari-jari  = ");
                double r = int.Parse(Console.ReadLine());
                double ll = pg.LuasLingkaran(r);
                double kl = pg.KllgLingkaran(r);
                Console.WriteLine("Luas lingkaran= " + ll);
                Console.WriteLine("Keliling lingkaran= " + kl);
            }

            else if (pil == 2)
            {
                Console.WriteLine();
                Console.WriteLine("---PERSEGI---");
                Console.Write("Input sisi = ");
                int s = int.Parse(Console.ReadLine());
                int lp = pg.LuasPersegi(s);
                int kp = pg.KllgPersegi(s);
                Console.WriteLine("Luas persegi = " + lp);
                Console.WriteLine("Keliling persegi = " + kp);
            }

            else if (pil == 3)
            {
                Console.WriteLine();
                Console.WriteLine("---MOD ANGKA---");
                Console.Write("Input angka = ");
                int a = int.Parse(Console.ReadLine());
                Console.Write("Input pembagi = ");
                int p = int.Parse(Console.ReadLine());
                int hasilmod = pg.ModAngka(a,p);

                if (hasilmod == 0)
                {
                    Console.WriteLine("Angka " + a + " % " + p + " adalah 0");
                }
                else
                {
                    Console.WriteLine("Angka " + a + " % " + p + " bukan 0, melainkan " + hasilmod);
                }
            }

            else if (pil == 4)
            {
                Console.WriteLine();
                Console.WriteLine("---PENGHASILAN ROKOK---");
                int hasil = pg.Rokok(100);
                int total = hasil * 500;
                Console.WriteLine("Jumlah batang rokok yang berhasil dirangkai adalah " + hasil);
                Console.WriteLine("Penghasilan 100 puntung rokok adalah Rp" + total);
            }

            else
            {
                Console.WriteLine("PILIHAN TIDAK ADA !!");
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
