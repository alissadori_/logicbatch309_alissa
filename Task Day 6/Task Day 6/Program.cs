﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Task_Day_6
{
    internal class Program
    {
        public void Soal1() // time conversion
        {
            Console.Clear();

            DateTime dt1 = new DateTime();
            Console.WriteLine("Masukkan waktu = ");
            var waktu = Console.ReadLine();
            Console.WriteLine();
            string waktu2 = DateTime.Parse(waktu).ToString("HH:mm:ss");
            Console.WriteLine(waktu2);
            Console.WriteLine();


            
          /* Cara 2
            Console.WriteLine("Input waktu = ");
            string[] waktu = Console.ReadLine().Split(":");

            int jam = int.Parse(waktu[0]);
            if(input[2].Contains("PM"))
            {
                waktu[2] = waktu[2].Substring(0,2)
                if(jam<12)
                {
                    waktu[0] = (jam+12).ToString()
                }
            }
            else if (waktu[2].Contains("AM"))
            {
                waktu[2] = waktu[2].Substring(0,2)
                if(jam==12)
                {   
                    waktu[0] = "00";
                }

            }

            Console.WriteLine($"{waktu[0]} : {waktu[1] : {waktu[2]}}");
          */
        }

        public void Soal2() // elsa dan dimas makan di restoran
        {
            Console.Clear();

            Console.Write("Total menu           = ");
            int totalMenu = int.Parse(Console.ReadLine());

            Console.Write("Index makanan alergi = ");
            int indexAlergi = int.Parse(Console.ReadLine());

            Console.Write("Harga menu           = ");
            string hargaMenu1= Console.ReadLine();
            string[] hargaMenu2 = hargaMenu1.Split(',');
            int[] hargaMenu3 = Array.ConvertAll(hargaMenu2, int.Parse);

            List<int> MenuList1 = hargaMenu3.ToList();
            List<int> MenuList2 = Array.ConvertAll(hargaMenu1.Split(","), int.Parse).ToList();

            int jumlah = 0;

            string dimakan = string.Join(" + ", MenuList1);

            int Length = MenuList1.Count;
            for(int i= 0; i < Length;i++)
            {
                jumlah = jumlah + MenuList1[i];
            }

            Console.Write("Uang elsa            = ");
            int uangElsa = int.Parse(Console.ReadLine());
            int sisaUang = uangElsa - ((jumlah - MenuList1[indexAlergi]) / 2);

            int bayar = (jumlah - MenuList1[indexAlergi]) / 2;

            Console.WriteLine();
            Console.WriteLine($"Total makanan yg dimakan elsa dan dimas adalah {dimakan} = {jumlah}");
            Console.WriteLine($"Makanan yg bisa elsa makan = {jumlah - MenuList1[indexAlergi]}. Karena {jumlah} - {MenuList1[indexAlergi]}");

            Console.WriteLine();

            if(sisaUang > 0)
            {
                Console.WriteLine("Elsa harus membayar = " + bayar);
                Console.WriteLine($"Sisa uang elsa      = {uangElsa-sisaUang}");
            }
            else if(sisaUang<0)
            {
                Console.WriteLine("Elsa harus membayar = " + bayar);
                Console.WriteLine($"Sisa uang elsa      = {uangElsa - sisaUang}");
            }
            else if(sisaUang==0)
            {
                Console.WriteLine("Uang elsa pas");
            }
            else
            {
                Console.WriteLine("Error");
            }

            Console.WriteLine();
            Console.WriteLine();
        }

        public void Soal3() // birthday candle
        {
            Console.Write("Input = "); string input = Console.ReadLine();
            string[] inputArrStr = input.Split(" ");

            int[] inputArrInt = Array.ConvertAll(inputArrStr, int.Parse);
            int Length = inputArrInt.Length;

            int temp = 0, count = 0;

            for(int i = 0; i < Length; i++)
            {
                for(int j = 0; j< Length-1; j++)
                {
                    if (inputArrInt[j] < inputArrInt[j + 1])
                    {
                        temp = inputArrInt[j];
                        inputArrInt[j] = inputArrInt[j + 1];
                        inputArrInt[j + 1] = temp;
                    }
                }
            }

            for (int i = 0; i < Length; i++)
            {
                if (inputArrInt[0] == inputArrInt[i])
                {
                    count++;
                }
            }

            string cetak = string.Join(",", inputArrInt);

            Console.WriteLine(cetak);
            Console.WriteLine("Output = " + count);
            Console.WriteLine();
    
        }

        public void Soal4() // change position of number from index array
        {
            int[] array = new int[5];
            int i,j,n,rot;

            Console.Clear();

            Console.Write("Masukkan banyak data   = "); n = int.Parse(Console.ReadLine());
            Console.Write("Masukkan banyak rotasi = "); rot = int.Parse(Console.ReadLine());

            Console.WriteLine("Input data array = ");
            array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            
            for(i=0;i<rot;i++)
            {
                int first = array[0];
                for(j=0;j<n-1;j++)
                {
                    array[j] = array[j + 1];
                }
                array[n - 1] = first;
                Console.Write(String.Join(",",array));
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        public void Soal5() // melewati rintangan
        {
            Console.Clear();

            Console.Write("Input k         = ");
            int k = int.Parse(Console.ReadLine());
            
            Console.Write("Input rintangan = ");
            string rintangan1 = Console.ReadLine();
            string[] rintangan2 = rintangan1.Split(" ");
            int[]  rintangan3= Array.ConvertAll(rintangan2, int.Parse);
            int Length = rintangan3.Length;

            int max = rintangan3[0], botol = 0;
            for (int i = 0; i < Length; i++)
            {
                if (rintangan3[i] > max)
                {
                    max = rintangan3[i];
                    if (k < max)
                    {
                        botol = max - k;
                    }
                    else
                    { 
                        botol= 0;
                    }
                }
            }

            Console.WriteLine($"{botol} botol ramuan ajaib");
            Console.WriteLine();
            Console.WriteLine();

        }

        public void Soal6() //satukan huruf yg sama, lalu pisahkan dengan koma
        {
            int i,j;
            string kalimat2="";
            char temp;
            char[] kalimat3;
            Console.Clear();

            Console.Write("Masukkan kalimat = ");
            string kalimat1 = Console.ReadLine();
 
            for(i=0;i<kalimat1.Length;i++)
            {
                if ((kalimat1[i] >= 'a' && kalimat1[i] <= 'z') || (kalimat1[i] >= 'A' && kalimat1[i] <= 'Z'))
                {
                    kalimat2 += kalimat1[i].ToString();
                }
            }

            kalimat3=kalimat2.ToCharArray();

            for (i = 0; i < kalimat3.Length; i++)
            {
                for (j = 0; j < kalimat3.Length - 1; j++)
                {
                    if (kalimat3[j] > kalimat3[j + 1])
                    {
                        temp = kalimat3[j];
                        kalimat3[j] = kalimat3[j + 1];
                        kalimat3[j + 1] = temp;
                    }
                }
            }

            /*Array.Sort(kalimat2);*/

            Console.WriteLine(String.Join(",", kalimat3));
            Console.WriteLine();
        }

        public void Soal7() //hitung lembah
        {
            int posisi = 0, lembah = 0, simbol = 0;
            
            Console.Write("Input perjalanan = ");
            string perjalanan1 = Console.ReadLine().ToUpper();
            char[] perjalanan2 = perjalanan1.ToCharArray();
            int Length = perjalanan2.Length;

            for(int i =0; i<Length; i++)
            {
                if (perjalanan2[i] == 'U')
                {
                    simbol++;
                }
                else if (perjalanan2[i] == 'D')
                {
                    simbol--;
                }

                if (posisi == 0 && simbol== -1)
                {
                    lembah++;
                }
            }

            Console.WriteLine($"Output = {lembah}");
            Console.WriteLine();
            Console.WriteLine();

        }

        public void MenuSoal()
        {
            int pil;
            Console.Clear();

            do
            {
                Console.WriteLine("----- Menu Soal Day 6 -----");
                Console.WriteLine("1. Soal 1");
                Console.WriteLine("2. Soal 2");
                Console.WriteLine("3. Soal 3");
                Console.WriteLine("4. Soal 4");
                Console.WriteLine("5. Soal 5");
                Console.WriteLine("6. Soal 6");
                Console.WriteLine("7. Soal 7");
                Console.WriteLine("8. Keluar");

                Console.WriteLine();

                Console.Write("Masukkan Pilihan = "); pil = int.Parse(Console.ReadLine());

                Console.WriteLine();

                switch (pil)
                {
                    case 1:
                        Soal1();
                        break;
                    case 2:
                        Soal2();
                        break;
                    case 3:
                        Soal3();
                        break;
                    case 4:
                        Soal4();
                        break;
                    case 5:
                        Soal5();
                        break;
                    case 6:
                        Soal6();
                        break;
                    case 7:
                        Soal7();
                        break;
                    default:
                        Console.WriteLine("Menu tidak tersedia");
                        break;
                }
            } while (pil != 8);
        }


        static void Main(string[] args)
        {
            Program pg = new Program();
            pg.MenuSoal();
        }
    }
}
